using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Discounts;
using Application.Products;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{

    public class ProductController : BaseController
    {
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetList()
        {

            return await Mediator.Send(new ProductList.Query());
        }
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> Details(Guid id)
        {
            return await Mediator.Send(new ProductDetails.Query { Id = id });
        }


        [HttpPost]
        public async Task<ActionResult<Unit>> Create(ProductCreate.Command command)
        {

            return await Mediator.Send(command);

        }
        [HttpPut("{id}")]
        public async Task<ActionResult<Unit>> Edit(Guid id, ProductEdit.Command command)
        {
            command.Id = id;
            return await Mediator.Send(command);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id)
        {
            return await Mediator.Send(new ProductDelete.Command { Id = id });
        }

        [HttpPost("{id}/addtobasket")]
        public async Task<ActionResult<Unit>> AddToBasket(Guid id)
        {

            return await Mediator.Send(new ProductAddToBasket.Command { Id = id });
        }    
    }
}