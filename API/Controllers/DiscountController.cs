using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Discounts;
using Application.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class DiscountController:BaseController
    { 
         [HttpGet]
        public async Task<ActionResult<List<ProductDiscountDto>>> List()
        {
            return await Mediator.Send(new DiscountList.Query());
        } 
        [HttpPost("creatediscountforproduct")]
        public async Task<ActionResult<Unit>> CreateDiscountForProduct(CreateDiscountForProduct.Command command)
        {
         
            return await Mediator.Send(command);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id)
        {
            return await Mediator.Send(new DiscountDelete.Command { Id = id });
        }
    }
}