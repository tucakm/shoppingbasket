using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Basket;
using Application.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class ShoppingBasketController:BaseController
    {
       [HttpGet]
        public async Task<ActionResult<BasketDto>> GetList()
        {

            return await Mediator.Send(new BasketListItems.Query());
        }

        [HttpDelete("clearuserbasket")]
        public async Task<ActionResult<Unit>> Delete()
        {
            return await Mediator.Send(new CleanUserBasket.Command());
        }

    }
}