using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Basket
{
    public class CleanUserBasket
    {
        public class Command : IRequest
        {

        }
        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                this._userAccessor = userAccessor;
                _context = context;

            }
            public async Task<Unit> Handle(Command request,
            CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());
               
                if (user.ShoppingBasket == null)
                    throw new RestException(HttpStatusCode.NotFound, new { User = "User shopping basket is empty" });

                if (user.ShoppingBasket.ShoppingBasketItem.Count > 0)
                    foreach (var item in user.ShoppingBasket.ShoppingBasketItem)
                        _context.Remove(item);

                _context.Remove(user.ShoppingBasket);
                user.ShoppingBasketId=null;
                var succes = await _context.SaveChangesAsync() > 0;

                if (succes) return Unit.Value;

                throw new Exception("Problem saving changes");


            }

        }
    }
}