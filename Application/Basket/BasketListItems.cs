using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Dtos;
using Application.Errors;
using Application.Interfaces;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Logging;
using System.Collections;

namespace Application.Basket
{
    public class BasketListItems
    {
        public class Query : IRequest<BasketDto> { }

        public class Handler : IRequestHandler<Query, BasketDto>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            private readonly IUserAccessor _userAccessor;
            private readonly ILogger<Handler> _logger;

            public Handler(DataContext context, IMapper mapper, IUserAccessor userAccessor, ILogger<Handler> logger)
            {
                this._logger = logger;
                this._userAccessor = userAccessor;
                this._mapper = mapper;
                _context = context;

            }

            public async Task<BasketDto> Handle(Query request,
             CancellationToken cancellationToken)
            {

                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());

                if (user.ShoppingBasket == null)
                    throw new RestException(HttpStatusCode.NotFound, new { User = "User shopping basket is empty" });

                var basket = _mapper.Map<Domain.ShoppingBasket, BasketDto>(user.ShoppingBasket);

                CheckIfItemSumIsCorrect(basket.ShoppingBasketItem);

                basket.Discounts = new List<ProductDiscountDto>();

                foreach (var item in basket.ShoppingBasketItem)
                {

                    var discount = await _context.Discount.Where(d => d.RequiredProductId == item.ProductId).FirstOrDefaultAsync();
                    if (discount != null)
                    {
                        if (item.Amount / discount.RequiredAmount >= 1)
                        {
                            GetRewardedProductFromShoppingItems(basket.ShoppingBasketItem, discount, item.Amount / discount.RequiredAmount);

                        }
                        basket.Discounts.Add(_mapper.Map<Discount, ProductDiscountDto>(discount));

                    }
                    basket.Total += item.Sum;
                }
                 //Logging everything into console
                LoggingBasketIntoConsole(basket);
                LogginDiscounts(basket.Discounts);

                return basket;
            }
            private void GetRewardedProductFromShoppingItems(ICollection<BasketItemDto> shoppingbasketItems, Discount discount, int numberOfDiscounted)
            {

                foreach (var thisitem in shoppingbasketItems)
                {
                    if (thisitem.ProductId == discount.RewardedProductId)
                    {
                        if (discount.DiscountType == DiscountType.Percent)
                            thisitem.Sum -= thisitem.ProductPrice * numberOfDiscounted * discount.RewardedAmount / 100;
                        else if (discount.DiscountType == DiscountType.NextFree)
                            thisitem.Sum = thisitem.ProductPrice * (thisitem.Amount - discount.RewardedAmount * numberOfDiscounted);

                    }

                }

            }
            private void CheckIfItemSumIsCorrect(ICollection<BasketItemDto> sbi)
            {
                //Another foreach loop just  to making sure that sum is correct, 
                //in case that price was edited while this item was in basket                
                foreach (var it in sbi)
                {
                    it.Sum = it.Sum != it.ProductPrice * it.Amount ? it.ProductPrice * it.Amount : it.Sum;
                }
            }
            private void LoggingBasketIntoConsole(BasketDto basketDto)
            {
               
                foreach (var sbi in basketDto.ShoppingBasketItem)
                {
                    _logger.LogInformation($"PRODUCT:{sbi.ProductName}---PRICE:${sbi.ProductPrice}---AMOUNT{sbi.Amount}---SUM{sbi.Sum}");
                }
                _logger.LogInformation($"TOTAL--------------------{basketDto.Total}");

            }
            private void LogginDiscounts(List<ProductDiscountDto> discounts)
            {
                foreach (var apdiscount in discounts)
                {
                    _logger.LogInformation($"DISCOUNT ----BUY {apdiscount.RequiredAmount} {apdiscount.RequiredProduct.Name} AND GET {apdiscount.RewardedAmount}-{apdiscount.DiscountType} OF {apdiscount.RewardedProduct.Name}");
                }
            }



        }

    }
}