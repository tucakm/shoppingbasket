using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using FluentValidation;
using MediatR;
using Persistence;

namespace Application.Products
{
    public class ProductEdit
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public decimal Price { get; set; }
        }
    
        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {

                _context = context;

            }
            public async Task<Unit> Handle(Command request,
            CancellationToken cancellationToken)
            {
                var product =await _context.Products.FindAsync(request.Id);

                if(product==null)
                   throw new RestException(HttpStatusCode.NotFound,new {product="Not Found"});

                product.Name=request.Name ?? product.Name;
                product.Price=request.Price!=0?request.Price:product.Price;            

                var succes = await _context.SaveChangesAsync() > 0;

                if (succes) return Unit.Value;

                throw new Exception("Problem saving changes");


            }

        }
    }
}