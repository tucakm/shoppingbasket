using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Products
{
    public class ProductCreate
    {
        public class Command : IRequest
        { 
            public Guid Id { get; set; }     
            public string Name { get; set; } 
            public decimal Price { get; set; }
        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Name).NotEmpty();
                RuleFor(x => x.Price).NotEmpty();
            }
        }
        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
                      public Handler(DataContext context)
            {
            
                _context = context;

            }
            public async Task<Unit> Handle(Command request,
            CancellationToken cancellationToken)
            {
                if (await _context.Products.Where(x => x.Name == request.Name).AnyAsync())
                    throw new RestException(HttpStatusCode.BadRequest, new { Product = "Product allredy exists" });

                var product = new Product
                {
                  Name=request.Name,
                  Price = request.Price                  
                };
                
                _context.Products.Add(product);            
         

                var succes = await _context.SaveChangesAsync() > 0;

                if (succes) return Unit.Value;

                throw new Exception("Problem saving changes");


            }
        }
    }
}