using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Application.Interfaces;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;
namespace Application.Products
{
    public class ProductAddToBasket
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }

        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                _userAccessor = userAccessor;
                _context = context;

            }
            public async Task<Unit> Handle(Command request,
            CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());

                var product = await _context.Products.FindAsync(request.Id);

                if (product == null)
                    throw new RestException(HttpStatusCode.NotFound, new { product = "Not Found" });


                if (!await _context.ShoppingBasket.Where(x => x.AppUserId == user.Id).AnyAsync())
                {
                    CreateShopingBasket(user);
                    if (await _context.SaveChangesAsync() == 0)
                        throw new Exception("Couldnt create basket for user");
                    var shoppingBasket = await _context.ShoppingBasket.Where(x => x.AppUserId == user.Id).FirstOrDefaultAsync();

                    if (user.ShoppingBasketId == null)
                        user.ShoppingBasketId = shoppingBasket.Id;

                }


                var shoppingBasketItem = await _context.ShoppingBasketItems
                        .Where(s => s.ShoppingBasketId == user.ShoppingBasketId && s.ProductId == product.Id).FirstOrDefaultAsync();

                shoppingBasketItem = shoppingBasketItem != null ? IncreaseProductAmount(shoppingBasketItem) :
                        CreateItemForBasket(user.ShoppingBasket, product);


                var succes = await _context.SaveChangesAsync() > 0;

                if (succes) return Unit.Value;

                throw new Exception("Problem adding item to bucket");


            }
            private Domain.ShoppingBasket CreateShopingBasket(AppUser user)
            {
                var shoppingBasketNew = new Domain.ShoppingBasket
                {
                    AppUser = user,
                    Total = 0,
                    CreatedAt = DateTime.Now,
                    Status = BasketStatus.Default
                };
                _context.ShoppingBasket.Add(shoppingBasketNew);

                return shoppingBasketNew;
            }
            private ShoppingBasketItem CreateItemForBasket(Domain.ShoppingBasket shoppingBasket, Product product)
            {
                var shoppingBasketItem = new ShoppingBasketItem
                {
                    Product = product,
                    Amount = 1,
                    Sum = product.Price,
                    ShoppingBasket = shoppingBasket

                };
                _context.ShoppingBasketItems.Add(shoppingBasketItem);
                return shoppingBasketItem;

            }
            private ShoppingBasketItem IncreaseProductAmount(ShoppingBasketItem shoppingBasketItem)
            {

                shoppingBasketItem.Amount++;
                shoppingBasketItem.Sum = shoppingBasketItem.Amount * shoppingBasketItem.Product.Price;
                return shoppingBasketItem;
            }



        }
    }
}