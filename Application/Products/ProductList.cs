using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Domain
{
    public class ProductList
    {
          public class Query : IRequest<List<Product>> { }

        public class Handler : IRequestHandler<Query, List<Product>>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                this._context = context;

            }
            public async Task<List<Product>> Handle(Query request,
            CancellationToken cancellationToken)
            {
                var products = await _context.Products.ToListAsync();

                return products;
            }
        
        }
    }
}