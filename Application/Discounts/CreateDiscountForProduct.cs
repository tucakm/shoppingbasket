using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Application.Validators;

namespace Application.Discounts
{
    public class CreateDiscountForProduct
    {
        public class Command : IRequest
        {
            public int? DiscountType { get; set; }
            public Guid RequiredProductId { get; set; }
            public int RequiredAmount { get; set; }
            public Guid? RewardedProductId { get; set; }
            public int RewardedAmount { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }

        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.RequiredProductId).NotEmpty();
                RuleFor(x => x.RequiredAmount).NotEmpty();
                RuleFor(x => x.RewardedAmount).NotEmpty();
                RuleFor(x => x.DiscountType).NotEmpty().InclusiveBetween(0, 1);
                RuleFor(x => x.StartDate).NotEmpty();
                RuleFor(x => x.EndDate).GreaterThan(x => x.StartDate).NotEmpty();

            }
        }
        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {

                _context = context;

            }
            public async Task<Unit> Handle(Command request,
            CancellationToken cancellationToken)
            {
                var product = await _context.Products.FindAsync(request.RequiredProductId);

                if (product == null)
                    throw new RestException(HttpStatusCode.NotFound, new { product = "Not Found" });

                if (await _context.Discount.Where(x => x.RequiredProductId == product.Id).AnyAsync())
                    throw new RestException(HttpStatusCode.BadRequest, new { discount = "Discount for this product allredy exists" });

                var discount = new Discount
                {
                    DiscountType = (DiscountType)request.DiscountType,
                    RequiredProductId = request.RequiredProductId,
                    RequiredAmount = request.RequiredAmount,
                    RewardedProductId = request.RewardedProductId != null ? request.RewardedProductId : request.RequiredProductId,
                    RewardedAmount = request.RewardedAmount,
                    StartDate = request.StartDate,
                    EndDate = request.EndDate
                };
                _context.Discount.Add(discount);

                var succes = await _context.SaveChangesAsync() > 0;

                if (succes) return Unit.Value;

                throw new Exception("Problem saving changes");


            }
        }

    }
}

