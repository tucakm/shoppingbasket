using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Dtos;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Discounts
{
    public class DiscountList
    {

        public class Query : IRequest<List<ProductDiscountDto>> { }

        public class Handler : IRequestHandler<Query, List<ProductDiscountDto>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                this._context = context;

            }
            public async Task<List<ProductDiscountDto>> Handle(Query request,
            CancellationToken cancellationToken)
            {
                var discounts = await _context.Discount.ToListAsync();

                return _mapper.Map<List<Discount>,List<ProductDiscountDto>>(discounts);
            }

        }


    }
}