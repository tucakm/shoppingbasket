using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Domain;

namespace Application.Dtos
{
    public class BasketDto
    {
        public Guid Id { get; set; }        
        public string AppUserId { get; set; }
        public decimal Total { get; set; }

     
        public DateTime CreatedAt { get; set; }
        //1 active 0 unactive
        public int Status {get;set;}

        [JsonPropertyName("shoppingbasketitems")]
        public ICollection<BasketItemDto> ShoppingBasketItem { get; set; }

        [JsonPropertyName("applicable discounts")]
        public List<ProductDiscountDto> Discounts { get; set; }   
       
       
    }
}