using Application.Dtos;
using AutoMapper;
using Domain;

namespace Application.Activities
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Domain.ShoppingBasket, BasketDto>();
            CreateMap<ShoppingBasketItem, BasketItemDto>()
                .ForMember(d => d.ProductId, o => o.MapFrom(s => s.Product.Id));               
            CreateMap<Discount,ProductDiscountDto>()
                .ForMember(d =>d.RequiredProduct, o=>o.MapFrom(s => s.RequiredProduct))
                .ForMember(d =>d.RewardedProduct, o=>o.MapFrom(s => s.RewardedProduct));
              
        }
    }
}