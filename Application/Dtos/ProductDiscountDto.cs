using System;
using Domain;

namespace Application.Dtos
{
    public class ProductDiscountDto
    {
        public Guid Id { get; set; }
        public Product RequiredProduct { get; set; }
        public Product RewardedProduct { get; set; }     
        public int RequiredAmount { get; set; }
        public int RewardedAmount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DiscountType DiscountType { get; set; }
        
    }
}