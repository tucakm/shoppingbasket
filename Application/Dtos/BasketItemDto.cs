using System;
using Domain;

namespace Application.Dtos
{
    public class BasketItemDto
    {
     
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice{get;set;}   
        public int Amount {get;set;}
        public decimal Sum{get;set;} 

      
       
    }
}