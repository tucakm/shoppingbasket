using System.Net.Http;
using Application.Interfaces;
using Infrastructure.Security;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Microsoft.AspNetCore.Mvc.Testing;
using API;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System;

namespace Tests
{
    public class TestBase
    {           

        public DataContext GetDataContext()
        {
            var builder = new DbContextOptionsBuilder<DataContext>()
                     .EnableSensitiveDataLogging().UseSqlite("DataSource=:memory:");
            var context = new DataContext(builder.Options);
            context.Database.OpenConnection();
            context.Database.EnsureCreated();

            return context;

        }
    

    }
}