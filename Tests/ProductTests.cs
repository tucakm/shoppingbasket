using Xunit;
using System.Threading.Tasks;
using Persistence;
using Microsoft.EntityFrameworkCore;
using Application.Products;
using Domain;
using System.Linq;
using System;
using Application.Errors;
using Microsoft.AspNetCore.Mvc;
using Application.Interfaces;
using Moq;
using Infrastructure.Security;

namespace Tests
{
    public class ProducTests : TestBase
    {
        [Fact]
        
        public async Task CreateProduct()
        {
            var context = GetDataContext();

            var command = new ProductCreate.Command { Name = "Chicken breast", Price = 29.55m };
            var handler = new ProductCreate.Handler(context);

            await handler.Handle(command, new System.Threading.CancellationToken());


            Assert.Single(context.Products.Where(p => p.Name == command.Name));
        }
        [Fact]
        public async Task EditProduct_ShouldThrowRestException()
        {
            var context = GetDataContext();

            var command = new ProductEdit.Command { Id = Guid.NewGuid() };
            var handler = new ProductEdit.Handler(context);

            await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        }

        [Fact]
        public async Task DeleteProduct_ShouldThrowRestException()
        {
            var context = GetDataContext();

            var command = new ProductDelete.Command { Id = Guid.NewGuid() };
            var handler = new ProductDelete.Handler(context);

            await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        }

        [Fact]
        public async Task ProductAddToBasket_ShouldThrowRestException()
        {
            var context = GetDataContext();
            var _userAccessor= new Mock<IUserAccessor>();
            
            var command = new ProductAddToBasket.Command { Id = Guid.NewGuid() };        
            var handler = new ProductAddToBasket.Handler(context,_userAccessor.Object);

            await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        }
    }
}

