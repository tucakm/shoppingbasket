using System;
using System.Threading.Tasks;
using Application.Discounts;
using Application.Errors;
using Xunit;

namespace Tests
{
    public class DiscountTest:TestBase
    {
        [Fact]
        public async Task DeleteDiscount_ExpectRestException()
        {
            var context = GetDataContext();   


            var command = new DiscountDelete.Command{Id=Guid.NewGuid()};
            var handler = new DiscountDelete.Handler(context);

            await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        }
    }
}