using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Identity;

namespace Persistence
{
    public class SeedData
    {
        public static async Task SeedDataForDB(DataContext context, UserManager<AppUser> userManager)
        {
            if (!userManager.Users.Any())
            {
                var users = new List<AppUser>
                {
                    new AppUser{
                        DisplayName="bob",
                        UserName="bob",
                        Email="bob@test.com"
                    },
                    new AppUser{
                        DisplayName="joshua",
                        UserName="joshua",
                        Email="joshua@test.com"
                    },
                       new AppUser{
                        DisplayName="sally",
                        UserName="sally",
                        Email="sally@test.com"
                    },
                           new AppUser{
                        DisplayName="jake",
                        UserName="jake",
                        Email="jake@test.com"
                    }
                    ,
                           new AppUser{
                        DisplayName="stipe",
                        UserName="stipe",
                        Email="stipe@test.com"
                    }
                };
                foreach (var user in users)
                {
                    await userManager.CreateAsync(user, "Pa$$w0rd");
                }
            }
            if (!context.Products.Any())
            {
                var products = new List<Product>{
                    new Product
                    {
                        Name="Butter",
                        Price=0.8m

                    },
                     new Product
                    {
                        Name="Milk",
                        Price=1.15m

                    },
                    new Product
                    {
                        Name="Bread",
                        Price=1.00m

                    }
                };
                await context.Products.AddRangeAsync(products);
                await context.SaveChangesAsync();
            }



        }

    }
}