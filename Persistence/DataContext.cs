using Microsoft.EntityFrameworkCore;
using Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Persistence
{
    public class DataContext:IdentityDbContext<AppUser>
    {
        public DataContext(DbContextOptions options):base(options)
        {           
            
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<ShoppingBasket> ShoppingBasket { get; set; }
        public DbSet<ShoppingBasketItem> ShoppingBasketItems {get;set;}
        public DbSet<Discount> Discount { get; set; }
        protected override void OnModelCreating(ModelBuilder builder){
            base.OnModelCreating(builder);
            
            builder.Entity<ShoppingBasketItem>()                           
                .HasOne(sbi => sbi.ShoppingBasket)
                .WithMany(sb => sb.ShoppingBasketItem)
                .HasForeignKey(sbi => sbi.ShoppingBasketId);

          
            builder.Entity<ShoppingBasket>()
                .HasOne(sb=>sb.AppUser)
                .WithOne(ap=>ap.ShoppingBasket)                            
                .HasForeignKey<ShoppingBasket>(sb => sb.AppUserId);
                       

            builder.Entity<Discount>()
                .HasOne(d => d.RequiredProduct)
                .WithOne();        
                          
        }
       
        
    }
}