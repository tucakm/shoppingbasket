using System;
using Microsoft.AspNetCore.Identity;

namespace Domain
{
    public class AppUser:IdentityUser
    {      
        public string DisplayName { get; set; }

        public Guid? ShoppingBasketId { get; set; }

        public virtual ShoppingBasket ShoppingBasket { get; set; }
    }
}