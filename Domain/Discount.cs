using System;

namespace Domain
{
    public class Discount
    {
        public Guid Id { get; set; }
        public Guid RequiredProductId { get; set; }
        public virtual Product RequiredProduct { get; set; }
        public int RequiredAmount { get; set; }
        public Guid? RewardedProductId { get; set; }
        public virtual Product RewardedProduct { get; set; }
        public int RewardedAmount { get; set; }
        public DiscountType DiscountType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }


    }
    public enum DiscountType
    {
        NextFree,
        Percent
    }
}