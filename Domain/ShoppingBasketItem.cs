using System;

namespace Domain
{
    public class ShoppingBasketItem
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int Amount { get; set; }
        public Guid ShoppingBasketId { get; set; }

        public decimal Sum { get; set; }
        public virtual ShoppingBasket ShoppingBasket { get; set; }
        

    }
}