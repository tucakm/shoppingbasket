using System;
using System.Collections.Generic;

namespace Domain
{
    public class ShoppingBasket
    {
        public Guid Id { get; set; }
        public string AppUserId { get; set; }
        public virtual AppUser AppUser { get; set; }
        public decimal Total { get; set; }
        public virtual ICollection<ShoppingBasketItem> ShoppingBasketItem { get; set; }
        public DateTime CreatedAt { get; set; }
        //1 active 0 unactive
        public BasketStatus Status {get;set;}

    }

    public enum BasketStatus{
       
        Finished,
         Default
    }
}